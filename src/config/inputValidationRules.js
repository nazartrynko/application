export default {
  email: {
    test: testEmail,
    priority: 2
  },
  required: {
    test: testRequired,
    priority: 1
  },
  number: {
    test: testNumber,
    priority: 2
  }
};

function testRequired(str) {
  return str !== '';
}

function testNumber(str) {
  return Number(str);
}

function testEmail(str) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(str);
}
