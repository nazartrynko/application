import {
    createStore,
    applyMiddleware,
    compose
} from 'redux';
import Immutable, {Map} from 'immutable';
import thunk from 'redux-thunk';

if (__DEV__) {
    const installDevTools = require('immutable-devtools');
    installDevTools(Immutable);
}

import reducer from './reducers';

export default function create(initialState = Map({})) {
    const enhancer = compose(
        applyMiddleware(thunk)
    );

    return createStore(reducer, initialState, enhancer);
}
