import {ActionConst} from 'react-native-router-flux';
import {Map, fromJS} from 'immutable';

const initialState = Map({
    scene: Map({})
});

export default function navigator(state = initialState, action = {}) {
    switch (action.type) {
        case ActionConst.FOCUS:
            return state.set('scene', fromJS(action.scene));

        default:
            return state;
    }
}
