import {
  combineReducers
} from 'redux-immutable';

import navigator from './navigator';
import user from './user';

export default combineReducers({
  navigator,
  user
});
