import {Map, fromJS} from 'immutable';

const SET_TEST = 'application/user/SET_TEST';

const initialState = fromJS({
  test: 'Initial test data'
});

export function setTest(data) {
  return {type: SET_TEST, data}
}

export default function userReducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_TEST:
      {
        return state.mergeDeep({
          ...action.data
        })
      }
    default:
      return state;
  }
}
