import React, {Component, PropTypes} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Animated,
  Platform
} from 'react-native';
import {colors, fonts} from 'styles';

import {SwipeView, Slide} from 'components/onboarding/slider';
import scrollAnimation from 'helpers/scrollAnimation';
import {height, width, bottom} from 'helpers/dimensions';

export default class SliderView extends Component {

  constructor() {
    super();
    this.handleAnimated = this.handleAnimated.bind(this);
  }

  state = {
    xOffset: new Animated.Value(0),
    onScroll: null,
    animateModal: false,
    animateDots: false,
    modalIsOpen: false
  };

  componentWillMount() {
    this.animatedZIndex = new Animated.Value(1);
  }

  handleAnimated(flag) {
    const toValue = flag
      ? -1
      : 1;
    const delay = flag
      ? 250
      : 0;
    const options = {
      toValue,
      delay,
      duration: 0
    }

    Animated.timing(this.animatedZIndex, options).start();
  }

  render() {
    const {xOffset} = this.state;

    const platform = Platform.OS;
    const interpolation = scrollAnimation.create(platform, 2, width);

    const dotsOpacity = interpolation.getScrollPosition(xOffset, 'dots');

    const animatedZIndex = {
      zIndex: this.animatedZIndex
    }

    const scrollHandler = Platform.OS === 'ios'
      ? {
        onScroll: Animated.event([
          {
            nativeEvent: {
              contentOffset: {
                x: xOffset
              }
            }
          }
        ]),
        scrollEventThrottle: 16
      }
      : {
        onPageScroll: ({
          nativeEvent: {
            offset,
            position
          }
        }) => {
          xOffset.setValue(position + offset);
        }
      }
    const dotPosition = {
      opacity: dotsOpacity,
      marginBottom: bottom
    }
    return (
      <View>
        <SwipeView style={{
          height: height
        }} loop={false} activeDotColor='#fff' activeDotStyle={dotPosition} dotStyle={dotPosition} {...scrollHandler}>
          <Slide slideStyle={styles.slide1} heading="Welcome to my Application"
                 text="This could be onboarding to some service"
                 scrollPosition={xOffset} index={0}/>
          <Slide slideStyle={styles.slide2} heading="Interpolation in use"
                 text="You can see this text's position being interpolated to the slide position" scrollPosition={xOffset}
                 index={1}/>
          <Slide slideStyle={styles.slide3} heading="Dots below"
                 text="Slides counter disappears before modal comes up" scrollPosition={xOffset}
                 index={2}/>
          <Slide slideStyle={styles.slide4} heading="Simulated sign up"
                 text="feel in phone number to continue" scrollPosition={xOffset} index={3} modal={true}
                 handleAnimated={this.handleAnimated}/>
        </SwipeView>
        <Animated.View style={[styles.row, animatedZIndex]}>
          <Text style={styles.loginInfo}>Have an account?{'\u00A0'}</Text>
          <TouchableOpacity onPress={() => {
          }}>
            <Text style={styles.loginButton}>Login</Text>
          </TouchableOpacity>
        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loginInfo: {
    ...fonts._16PxGilroyMedium,
    color: colors.white,
    opacity: 0.45
  },
  loginButton: {
    ...fonts._16PxGilroyMedium,
    color: colors.white,
    height: 25
  },
  row: {
    position: 'absolute',
    top: 40,
    left: 0,
    right: 0,
    zIndex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: colors.transparent
  },
  termsContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  terms: {
    textAlign: 'center',
    color: colors.white,
    ...fonts._12PxGilroyMedium,
    paddingTop: height / 40
  },
  slide1: {
    backgroundColor: colors.slide1
  },
  slide2: {
    backgroundColor: colors.slide2
  },
  slide3: {
    backgroundColor: colors.slide3
  },
  slide4: {
    backgroundColor: colors.slide5
  }
});
