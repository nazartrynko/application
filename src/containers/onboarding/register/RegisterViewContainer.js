import {
  bindActionCreators
} from 'redux';
import {
  connect
} from 'react-redux';
import RegisterView from './RegisterView';
import {setTest} from 'reducers/user';

const mapStateToProps = state => ({
  test: state.getIn(['user', 'test'])
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setTest
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RegisterView);
