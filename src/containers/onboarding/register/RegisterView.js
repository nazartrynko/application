import React, {Component} from 'react';
import {
  View,
  Keyboard,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import TextField from 'react-native-md-textinput';
import {colors, fonts} from 'styles';
import {Header, CircleButton} from 'components/shared';
import {validate, saveField} from 'helpers/validation';
import autobind from 'autobind-decorator';

export default class CredentialsView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fields: {
        email: {
          value: '',
          isValid: false
        },
        password: {
          value: '',
          isValid: false
        }
      },
      isFormValid: false
    }
    // this.handleBackAction = this.handleBackAction.bind(this);
  }

  @autobind
  onChangeText(text, ref) {
    const {rules} = this.refs[ref].props;
    const {isValid, errorMessage} = validate(text, rules);
    saveField.call(this, ref, text, isValid);
  }

  @autobind
  submit() {

  }

  componentWillMount() {

  }

  render() {
    const {isFormValid, fields: {email, password}} = this.state;

    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView ref='scroll' keyboardShouldPersistTaps="always" style={styles.contentContainer}>
          <View style={styles.headingContainer}>
            <Header value={'Create your credentials'} color={colors.black} style={styles.heading}/>
          </View>
          <View style={styles.formContainer}>
            <TextField value={email.value} rules={[
              {title: 'required', errorMessage: 'Phone number is required'},
              {title: 'email', errorMessage: 'Invalid email format'},
            ]} ref="email" onChangeText={text => {
              this.onChangeText(text, 'email');
            }} height={45} label={'Email'} highlightColor={colors.black} style={styles.input}/>
          </View>
          <View style={styles.formContainer}>
            <TextField secureTextEntry={true} value={password.value} rules={[
              {title: 'required', errorMessage: 'Password is required'}
            ]} ref="password" onChangeText={text => {
              this.onChangeText(text, 'password');
            }} height={45} label={'Password'} highlightColor={colors.black} style={styles.input}/>
          </View>
          <View style={[styles.btnContainer]}>
            <CircleButton iconSize={20} onPress={this.submit} form={true} isDisabled={isFormValid !== 1}/>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }

  componentDidMount() {
    // set value of progress bar
    // this.props.show();
    // this.props.set(25);
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative'
  },
  contentContainer: {},
  headingContainer: {
    paddingHorizontal: 20,
    paddingVertical: 25
  },
  formContainer: {
    marginVertical: 10,
    marginHorizontal: 25
  },
  heading: {
    textAlign: 'left'
  },
  btnContainer: {
    marginTop: 30,
    paddingHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 20
  },
  input: {
    ...fonts._22PxGilroyMedium,
  }
});
