import React from 'react';
import {Scene} from 'react-native-router-flux';

import RegisterViewContainer from './register/RegisterViewContainer';
import SliderView from './slider/SliderView';
import config from 'config';

const sceneWithPadding = {
  paddingTop: config.navbar.height
};

export default (
  <Scene
    key="onboarding">
    <Scene
      hideNavBar={true}
      key="slider"
      component={SliderView}/>
    <Scene
      initial={true}
      key="register"
      sceneStyle={sceneWithPadding}
      component={RegisterViewContainer}/>
  </Scene>
);
