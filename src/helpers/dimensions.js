import {Dimensions, Platform} from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';

const {width} = Dimensions.get('window');
const statusBarHeight = ExtraDimensions.get('STATUS_BAR_HEIGHT');
const height = ExtraDimensions.get('REAL_WINDOW_HEIGHT') - statusBarHeight
  - ExtraDimensions.get('SOFT_MENU_BAR_HEIGHT');
const bottom = Dimensions.get('window').height - height;

export {width, height, bottom, statusBarHeight};
