import formRules from 'config/inputValidationRules';

export function validate(text, rules) {
  const errors = [];
  let error = null;

  rules.forEach(rule => {
    const formRule = formRules[rule.title];

    if (formRule) {
      const isValid = formRule.test(text);

      if (!isValid) {
        errors.push({
          priority: formRule.priority,
          message: rule.errorMessage
        })
      }
    } else console.error('Incorrect type of form rule');
  });

  if (errors.length > 0) {
    error = errors.reduce((prev, current) => (prev.priority < current.priority) ? prev : current);
  }

  return {
    isValid: errors.length === 0,
    errorMessage: error && error.message
  };
}

export function saveField(field, value, isValid) {
  this.setState(state => ({
    ...state,
    fields: {
      ...state.fields,
      [field]: {
        value,
        isValid
      }
    }
  }), () => {
    const isFormValid = checkFormValidity.call(this);
    debugger;
    this.setState(state => ({
      ...state,
      isFormValid
    }));
  });
}

function checkFormValidity() {
  const fields = this.state.fields;

  return Object.keys(fields).reduce((res, prop) => !fields[prop].isValid
    ? res * 0
    : res, 1);
}
