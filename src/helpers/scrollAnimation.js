export default function InterpolationFactory() {
  // output ranges
  this.heading = [100, 0, -100];
  this.description = [200, 0, -200];
  this.dots = [1, 1, -3];
}

InterpolationFactory.prototype.getScrollPosition = function(scrollPosition, type) {
  const outputRange = this[type];

  return outputRange ? scrollPosition.interpolate({
    outputRange,
    inputRange: this.inputRange,
    extrapolate: 'clamp'
  }) : null;
}

InterpolationFactory.prototype.getTriggerPoint = function() {
  return this.triggerPoint;
}

InterpolationFactory.create = function(platform, ...args) {

  if(typeof InterpolationFactory.instance === 'object') {
    return InterpolationFactory.instance;
  }

  const Factory = InterpolationFactory[platform];

  if (!Factory) {
    return null;
  }

  if (typeof Factory.prototype.getScrollPosition !== 'function') {
    Factory.prototype = new InterpolationFactory();
  }

  return new Factory(...args);
}

InterpolationFactory.ios = function(index, screenWidth) {
  const screenPosition = index * screenWidth;
  this.inputRange = [screenPosition - screenWidth, screenPosition, screenPosition + screenWidth];
  this.triggerPoint = index * screenWidth - screenWidth / 4;
};

InterpolationFactory.android = function(index) {
  this.inputRange = [index - 1, index, index + 1];
  this.triggerPoint = index - index / 15;
};
