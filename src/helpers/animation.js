import {Animated} from 'react-native';

export function createAnimatedGetter(options = {tension: 460, friction: 44, velocity: 0}, type = 'spring') {
  return (variable, value) => Animated[type](
    variable, {...options, toValue: value}
  );
}

export function createAnimationGetter(animatedSpringGetter) {
  return (variable, value) => (value !== undefined) && animatedSpringGetter(variable, value)
}
