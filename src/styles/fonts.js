export const fonts = {
  _26PxGilroyExtrabold: {
    fontFamily: "Gilroy-ExtraBold",
    fontSize: 26,
    lineHeight: 26.0,
    letterSpacing: -0.3
  },
  _22PxGilroyMedium: {
    fontFamily: "Gilroy-Medium",
    fontSize: 22,
    lineHeight: 22.0,
    letterSpacing: -0.3
  },
  _18PxGilroyMedium: {
    fontFamily: "Gilroy-Medium",
    fontSize: 18,
    lineHeight: 18.0,
    letterSpacing: -0.3
  },
  _16PxGilroyMedium: {
    fontFamily: "Gilroy-Medium",
    fontSize: 16,
    lineHeight: 16.0,
    letterSpacing: -0.27
  },
  _14PxGilroyExtrabold: {
    fontFamily: "Gilroy-ExtraBold",
    fontSize: 14,
    letterSpacing: 0.82
  },
  _14PxGilroyMedium: {
    fontFamily: "Gilroy-Medium",
    fontSize: 14,
    lineHeight: 14.0,
    letterSpacing: -0.3
  },
  _12PxGilroyExtrabold: {
    fontFamily: "Gilroy-ExtraBold",
    fontSize: 12,
    letterSpacing: 0.7
  },
  _12PxGilroyMedium: {
    fontFamily: "Gilroy-Medium",
    fontSize: 12,
    lineHeight: 12.0,
    letterSpacing: -0.2
  },
  _12PxGilroyBold: {
    fontFamily: "Gilroy-Bold",
    fontSize: 12,
    lineHeight: 18,
    letterSpacing: -0.2
  },
  _11PxGilroyBold: {
    fontFamily: "Gilroy-Bold",
    fontSize: 11,
    letterSpacing: 0.6
  }
};
