import React, {Component, PropTypes} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {colors, fonts} from 'styles';
import {height, width, ios} from 'helpers/dimensions';


export function Description({
  value, color, style
}) {
  return (
    <Text style={[styles.text, {color: color}, style]}>{value}</Text>
  )
}

const styles = StyleSheet.create({
  text: {
    ...fonts._18PxGilroyMedium,
    lineHeight: 22.0,
    textAlign: "center"
  }
});
