import React, {Component, PropTypes} from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {colors, fonts} from 'styles';
import {height, width, ios} from 'helpers/dimensions';


export function Header({
  value, color, style
}) {
  return (
    <Text style={[styles.text, {color: color}, style]}>{value}</Text>
  )
}

const styles = StyleSheet.create({
  text: {
    ...fonts._26PxGilroyExtrabold,
    textAlign: "center",
    height: 50
  }
});
