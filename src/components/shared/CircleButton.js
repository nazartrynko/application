import React, {Component} from 'react';
import {View, Image, Animated, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';

import {colors} from 'styles';

function getInterpolatedValue(value, inputRange, outputRange) {
  return value.interpolate({inputRange, outputRange});
}

export class CircleButton extends Component {

  static propTypes = {
    onPress: PropTypes.func,
    isDisabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.number]),
  }

  componentWillMount() {
    this.disabled = true;
    const inputRange = [0, 1];
    const value = this.animateEnabled = new Animated.Value(0);
    this.disabledIcon = getInterpolatedValue(value, inputRange, [1, 0]);
    this.enabledIcon = getInterpolatedValue(value, inputRange, [0, 1]);
    this.animateEnabledSize = getInterpolatedValue(value, inputRange, [20, 50]);
    this.animateDisabledSize = getInterpolatedValue(value, inputRange, [50, 20]);
    this.animateDisabled = getInterpolatedValue(value, inputRange, [1, 0]);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isDisabled && !this.disabled) {
      this.disabled = true;
      this.handleDisabled(true);
    } else if (!nextProps.isDisabled && this.disabled) {
      this.disabled = false;
      this.handleDisabled(false);
    }
  }

  handleDisabled(disabled) {
    Animated.spring(this.animateEnabled, {
      tension: 460,
      friction: 44,
      velocity: 0,
      toValue: disabled ? 0 : 1
    }).start();
  }

  render() {

    const {iconName, onPress, isDisabled} = this.props;

    const enabledIcon = {
      transform: [
        {scale: this.enabledIcon}
      ]
    }
    const disabledIcon = {
      transform: [
        {scale: this.disabledIcon}
      ]
    }
    const animatedIcon = {
      transform: [
        {scale: this.animatedIcon}
      ]
    }

    const icon = (animated) => !iconName
      ? <Animated.View style={animated}>
        <Icon name="ios-arrow-round-forward" color="white" size={30} />
      </Animated.View>
      : null

    const animateEnabled = {
      opacity: this.animateEnabled,
      height: this.animateEnabledSize,
      width: this.animateEnabledSize
    }

    const animateDisabled = {
      opacity: this.animateDisabled,
      height: this.animateDisabledSize,
      width: this.animateDisabledSize
    }

    return (
      <TouchableOpacity style={styles.wrapper} onPress={onPress} disabled={isDisabled}>
        <Animated.View style={[styles.disabledCircle, styles.enabledCircle, animateEnabled]}>
          {icon(enabledIcon)}
        </Animated.View>
        <Animated.View style={[styles.disabledCircle, animateDisabled]}>
          {icon(disabledIcon)}
        </Animated.View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    alignItems: 'center',
    backgroundColor: 'transparent'
  },
  disabledCircle: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: 50,
    backgroundColor: colors.silver,
    borderRadius: 50
  },
  enabledCircle: {
    position: 'absolute',
    backgroundColor: colors.bluePurple
  }

});
