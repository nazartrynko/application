import React, {Component, PropTypes} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

import {colors, fonts} from 'styles';
import {height, width} from 'helpers/dimensions';

export function SmallButton({value, resend, onPress}) {
  return (
    <TouchableOpacity disabled={!resend} onPress={onPress}>
      <View style={styles.button}>
        <Text style={[
          styles.text, {
            color: resend
              ? colors.black
              : colors.coolGrey
          }
        ]}>{value}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  text: {
    ...fonts._12PxGilroyMedium,
    textAlign: "center",
    paddingHorizontal: 15
  },
  button: {
    height: 26,
    borderRadius: 3,
    backgroundColor: colors.silver,
    justifyContent: "center"
  }
});
