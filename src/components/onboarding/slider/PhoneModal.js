import React, {Component} from 'react';
import {
  View,
  Text,
  Keyboard,
  Animated,
  Platform,
  StyleSheet,
  PanResponder,
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';

import {colors, fonts} from 'styles';
import {height, width} from 'helpers/dimensions';
import {SmallButton, CircleButton, Header} from 'components/shared';
import scrollAnimation from 'helpers/scrollAnimation';
import PhoneInput from './PhoneInput';
import {SmsInput} from './SmsInput';

export class PhoneModal extends Component {

  constructor() {
    super();
    this.listener = this.listener.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleSms = this.handleSms.bind(this);
    this.handleResend = this.handleResend.bind(this);
    this.handleChangeValue = this.handleChangeValue.bind(this);
    this.handleSmsCodeValue = this.handleSmsCodeValue.bind(this);
  }

  state = {
    value: '',
    smsConfirm: false,
    animateModal: false,
    resend: false,
    timerValue: '0:10',
    smsValue: '',
    smsCodeValue: '',
    isDisabled: true,
    containerTranslateX: new Animated.Value(0)
  };

  interval = null;

  componentWillMount() {
    this.modalY = new Animated.Value(height);
    this.modalTop = new Animated.Value(height / 1.3);
    this.phoneInputY = new Animated.Value(height / 1.33); // padding on phone input
    this.headingOpacity = new Animated.Value(0);
    this.keyboardY = new Animated.Value(height);
    this.agreementY = new Animated.Value(height / 1.13);
    this.slideContainer = new Animated.Value(0);
    this.slideOpacity = new Animated.Value(1);
    this.closeOpacity = new Animated.Value(0);
    this.coverZIndex = new Animated.Value(2);
    this.slideContainer.addListener(this.listener);

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,

      onPanResponderMove: Animated.event([
        null, {
          dy: this.state.containerTranslateX
        }
      ]),

      onPanResponderRelease: () => {
        this.openModal();
        this.input.focus();
      },
    });
    this._panResponderForTerms = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onPanResponderMove: Animated.event([
        null, {
          dy: this.state.containerTranslateX
        }
      ])
    });
  }

  listener({value}) {
    if (value < -190) {
      this.slideContainer.setValue(375);
      Animated.parallel([
        Animated.spring(this.slideContainer, {
          tension: 400,
          friction: 50,
          velocity: 5,
          toValue: 0
        }),
        Animated.timing(this.slideOpacity, {
          duration: 280,
          toValue: 1
        }),
        Animated.timing(this.closeOpacity, {
          duration: 280,
          toValue: 1
        })
      ]).start();
      this.setState({smsConfirm: true, timerValue: '0:10'});
      this.startTimer();
    }
  }

  componentDidMount() {
    this.state.containerTranslateX.addListener(({value}) => {
      if (value < -15 && !this.modalIsOpen) {
        this.openModal();
        this.input.focus();
      }
    })
  }

  handleChangeValue(value) {
    if (value !== undefined) {
      this.setState({value});
      if (value.length === 12) {
        this.setState({isDisabled: false})
      } else if(!this.state.isDisabled) {
        this.setState({isDisabled: true})
      }
    }
  }

  handleSms() {
    Keyboard.dismiss();
    Animated.parallel([
      Animated.spring(this.slideContainer, {
        tension: 400,
        friction: 50,
        velocity: 5,
        toValue: -200
      }),
      Animated.timing(this.slideOpacity, {
        duration: 280,
        toValue: 0
      }),
      Animated.timing(this.closeOpacity, {
        duration: 280,
        toValue: 0
      })
    ]).start();
  }

  handleSmsCodeValue(smsCodeValue) {
    this.setState({smsCodeValue});
    if (smsCodeValue.length === 4) {
        clearInterval(this.interval);
        Keyboard.dismiss();
        Actions.register();
    }
  }

  handleResend() {
    this.setState({timerValue: '0:10', resend: false});
    this.startTimer();
  }

  startTimer() {
    let value = 10;

    this.interval = setInterval(() => {
      if (value <= 10 && value > 0) {
        value -= 1;
        this.setState({
          timerValue: '0:0' + value
        })
      } else if (value > 0) {
        value -= 1;
        this.setState({
          timerValue: '0:' + value
        })
      } else {
        this.setState({resend: true});
        clearInterval(this.interval);
      }
    }, 1000)
  }

  closeModal(fromKeyboard) {
    if (this.modalIsOpen) {
      this.modalIsOpen = false;
      Keyboard.dismiss();
      if (!fromKeyboard) {
        Keyboard.dismiss();
      }
      Animated.parallel([

        Animated.timing(this.modalY, {
          tension: 460,
          friction: 44,
          duration: 350,
          toValue: height
        }),
        Animated.timing(this.keyboardY, {
          tension: 460,
          friction: 44,
          duration: 350,
          toValue: height
        }),
        Animated.timing(this.phoneInputY, {
          tension: 460,
          friction: 44,
          duration: 350,
          toValue: height / 1.33
        }),
        Animated.timing(this.agreementY, {
          tension: 460,
          friction: 44,
          duration: 350,
          toValue: height / 1.13
        }),
        Animated.timing(this.headingOpacity, {
          tension: 460,
          friction: 44,
          duration: 15,
          toValue: 0
        }),
        Animated.timing(this.closeOpacity, {
          tension: 460,
          friction: 44,
          duration: 15,
          toValue: 0
        }),
        Animated.timing(this.coverZIndex, {
          tension: 460,
          friction: 44,
          duration: 15,
          toValue: 2
        })
      ]).start();
    }
  }

  openModal() {
    if (!this.modalIsOpen) {
      this.modalIsOpen = true;
      Animated.parallel([
        Animated.timing(this.modalY, {
          tension: 460,
          friction: 44,
          duration: 300,
          toValue: 0
        }),
        Animated.timing(this.keyboardY, {
          tension: 460,
          friction: 44,
          duration: 300,
          toValue: height / 1.61
        }),
        Animated.timing(this.phoneInputY, {
          tension: 460,
          friction: 44,
          duration: 300,
          toValue: height / 3.51 - height / 20
        }),
        Animated.timing(this.agreementY, {
          tension: 460,
          friction: 44,
          duration: 300,
          toValue: height / 1.85
        }),
        Animated.timing(this.headingOpacity, {
          tension: 460,
          friction: 44,
          duration: 15,
          delay: 200,
          toValue: 1
        }),
        Animated.timing(this.closeOpacity, {
          tension: 460,
          friction: 44,
          duration: 15,
          delay: 200,
          toValue: 1
        }),
        Animated.timing(this.coverZIndex, {
          tension: 460,
          friction: 44,
          duration: 15,
          toValue: -1
        })
      ]).start();
    }
  }

  toggleModal(flag) {
    const toValue = flag
      ? 0
      : height / 4;
    const options = {
      toValue,
      tension: 10,
      friction: 7,
      duration: 350
    }

    Animated.timing(this.modalTop, options).start();
  }

  render() {
    const {
      value,
      resend,
      isDisabled,
      smsConfirm,
      smsValue,
      timerValue,
      smsCodeValue
    } = this.state;
    const {scrollPosition} = this.props;
    const index = 3;
    const triggerFactory = scrollAnimation.create(Platform.OS, index, width);
    const translateY = scrollPosition.addListener(({value}) => {

      const flag = value > triggerFactory.getTriggerPoint(Platform.Os, index);

      if (flag && !this.opened) {
        this.toggleModal(flag);
        this.opened = !this.opened;
      } else if (!flag && this.opened) {
        if (this.opened) {
          this.closeModal();
        }
        this.toggleModal(flag);
        this.opened = !this.opened;
      }
    });

    const animatedScale = {
      opacity: this.submitButtonScale
    }
    const modalY = {
      top: this.modalY
    }
    const modalTop = {
      top: this.modalTop
    }
    const phoneInputY = {
      top: this.phoneInputY
    }
    const headingOpacity = {
      opacity: this.headingOpacity
    }

    const keyboardY = {
      top: this.keyboardY
    }
    const agreementY = {
      top: this.agreementY
    }

    const slideOpacity = {
      opacity: this.slideOpacity
    }

    const coverZIndex = {
      zIndex: this.coverZIndex
    }

    const slideContainer = {
      transform: [
        {
          translateX: this.slideContainer
        }
      ]
    }
    const closeOpacity = {
      opacity: this.closeOpacity
    }

    return (
      <Animated.View style={[styles.modal, modalTop]}>
        {/* Background */}
        <Animated.View style={[styles.background, modalY]} />
        {/* Close button */}
        <Animated.View style={[styles.closeWrapper, closeOpacity]}>
          <TouchableOpacity onPress={this.closeModal} style={styles.closeModal}>
            <View style={styles.closeContainer}>
              <Icon name="ios-close" color="#E4E4E6" size={50} />
            </View>
          </TouchableOpacity>
        </Animated.View>
        <Animated.View style={[
          {
            position: 'absolute',
            top: 0,
            height: height,
            width: width
          },
          slideContainer,
          slideOpacity
        ]}>
          {/* Heading */}
          <Animated.View style={[styles.headingContainer, headingOpacity]}>
            <Header value={!smsConfirm
              ? 'Your phone number'
              : '+44' + ' ' + value} color={colors.black}/>
          </Animated.View>
          {/* Phone Input */}
          {/* added cover view for modal to make it evenly responsive for touch and swipe up */}
          <Animated.View {...this._panResponder.panHandlers} style={[
            styles.phoneInputContainer,
            phoneInputY, {
              backgroundColor: colors.transparent
            }, coverZIndex
          ]}/>
          <Animated.View style={[styles.phoneInputContainer, phoneInputY]}>
            {!smsConfirm
              ? <PhoneInput onRef={ref => (this.input = ref)} type="masked" onFocus={this.openModal} onChangeText={this.handleChangeValue}/>
              : <SmsInput onRef={ref => (this.input = ref)} animated={headingOpacity} onFocus={this.openModal} handleSmsCodeValue={this.handleSmsCodeValue}/>
            }
          </Animated.View>

          {/* Terms and Conditions */}
          <Animated.View style={[styles.agreementContainer, agreementY]} {...this._panResponder.panHandlers}>
            <Text style={styles.agreement} {...this._panResponder.panHandlers}>
              By signing up to Application you agree to our
            </Text>
            <Text style={styles.terms} {...this._panResponderForTerms.panHandlers}>
              Terms and Conditions
            </Text>

          </Animated.View>
          {/* Submit/resend button */}
          <Animated.View style={[styles.submitButtonContainer, headingOpacity]}>
            {!smsConfirm
              ? <CircleButton isDisabled={isDisabled} onPress={this.handleSms} />
              : <SmallButton onPress={this.handleResend} value={!resend
                ? "Resend code: " + timerValue
                : "Resend code"} resend={resend}/>
            }
          </Animated.View>
        </Animated.View>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    left: 0,
    flex: 1,
    width: width,
    height: height,
    position: 'absolute',
    justifyContent: 'space-between',
    backgroundColor: 'transparent'
  },
  background: {
    width: width,
    height: height,
    backgroundColor: colors.white
  },
  closeWrapper: {
    position: 'absolute',
    zIndex: 10,
    top: 10,
    right: 0,
    height: 50,
  },
  headingContainer: {
    flex: 1,
    position: 'absolute',
    top: height / 7.4,
    width: width,
    alignItems: 'center',
  },
  closeModal: {
    width: 50,
    height: 80,
    zIndex: 100,
  },
  closeContainer: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    paddingRight: width / 20,
  },
  phoneInputContainer: {
    backgroundColor: colors.white,
    height: height / 3.9,
    width: width,
    borderRadius: 10,
    position: 'absolute',
    paddingLeft: width / 5.76,
  },

  phoneInputWrapper: {
    borderBottomColor: colors.silver,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: width / 15,
    height: 55,
    alignItems: 'flex-start'
  },
  phoneInput: {
    ...fonts._22PxGilroyMedium,
    fontSize: 22 * width / 350,
    width: width * .40,
    color: colors.black,
    height: 50
  },
  agreementContainer: {
    position: 'absolute',
    height: 60,
    width: width,
    zIndex: 3
  },
  submitButtonContainer: {
    flex: 1,
    width: width,
    alignItems: 'center',
    position: 'absolute',
    top: height / 2.3,
    zIndex: 3
  },
  agreement: {
    ...fonts._12PxGilroyMedium,
    color: colors.coolGrey,
    textAlign: 'center',
    width: width * 0.6,
    alignSelf: 'center'
  },
  terms: {
    ...fonts._12PxGilroyBold,
    color: colors.black,
    textAlign: 'center',
    alignSelf: 'center',
    width: width * 0.5,
    height: 30
  }
});
