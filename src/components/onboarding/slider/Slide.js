import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  Animated,
  Dimensions,
  StyleSheet,
  LayoutAnimation,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback
} from 'react-native';
import PropTypes from 'prop-types';

import getLayout from 'helpers/animation';
import {colors, fonts} from 'styles';
import {PhoneModal} from 'components/onboarding/slider';
import {Header, Description} from 'components/shared';
import {height, width} from 'helpers/dimensions';
import scrollAnimation from 'helpers/scrollAnimation';

export class Slide extends Component {

  constructor() {
    super();
    this.navigateFromModal = this.navigateFromModal.bind(this);
  }

  state = {
    isWhite: false
  }

  static propTypes = {
    slideStyle: PropTypes.number.isRequired,
    heading: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    summary: PropTypes.bool
  }
  componentWillMount() {
    this.agreementY = new Animated.Value(1);
  }

  navigateFromModal(value) {
    return new Promise((resolve, reject) => {
      this.setState({isWhite: value}, resolve);
    })
  }

  render() {

    const {
      text,
      index,
      modal,
      summary,
      heading,
      slideStyle,
      handleAnimated,
      scrollPosition
    } = this.props;
    const {isWhite} = this.state;
    const ScrolledView = this.ScrolledView;
    var translateHeadingX;
    var translateDescriptionX;

    if (scrollPosition) {
      const interpolation = scrollAnimation.create(Platform.OS, index, width);

      translateHeadingX = interpolation.getScrollPosition(scrollPosition, 'heading');
      translateDescriptionX = interpolation.getScrollPosition(scrollPosition, 'description');
    }

    return (
      <View style={[styles.slide, slideStyle, isWhite ? {backgroundColor: colors.white} : null]}>
        <View style={styles.upperContainer}>
          <View style={styles.loginInfoContainer}/>
          <View style={styles.description}>
            <ScrolledView scrollPosition={scrollPosition} translateX={translateHeadingX}>
              <Header value={heading} color={colors.white} style={styles.heading} />
            </ScrolledView>
            <ScrolledView scrollPosition={scrollPosition} translateX={translateDescriptionX}>
              <Description value={text} color={colors.white} style={styles.heading} />
            </ScrolledView>
          </View>
        </View>

        <TouchableWithoutFeedback style={styles.lowerContainer}>
          <View style={{
            flex: 4
          }}>
            {this.props.children}
          </View>
        </TouchableWithoutFeedback>
        {modal
          ? <PhoneModal navigateFromModal={this.navigateFromModal} handleAnimated={handleAnimated} scrollPosition={scrollPosition} />
          : null}
      </View>
    );
  }

  ScrolledView({
    scrollPosition,
    translateX,
    children
  }) {
    return <Animated.View style={scrollPosition && {
      transform: [{
          translateX
        }]
    }}>
      {children}
    </Animated.View>
  }
}

const styles = StyleSheet.create({
  wrapper: {
    justifyContent: 'space-around'
  },
  heading: {
    height: 50,
    marginBottom: height / 50
  },
  button: {
    backgroundColor: 'green',
    alignItems: 'center',
    height: 60,
    justifyContent: 'center'
  },
  upperContainer: {
    flex: 6,
    justifyContent: 'space-around'
  },
  loginInfoContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  description: {
    flex: 3,
    marginHorizontal: width / 15
  },
  lowerContainer: {
    flex: 5,
    justifyContent: 'flex-end'
  },
  slide: {
    flex: 1,
    alignItems: 'center'
  },
  emptySummaryContainer: {
    flex: 3
  },
  termsContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  terms: {
    color: '#fff',
    textAlign: 'center'
  },
  footer: {
    flex: 1
  },
  descriptionText: {
    ...fonts._14PxGilroyMedium,
    lineHeight: 23,
    textAlign: 'center',
    color: colors.white,
    marginHorizontal: width / 14
  },
  loginButton: {
    color: colors.white,
    paddingBottom: 10
  },
  play: {
    height: 50,
    width: 50,
    position: 'absolute',
    top: height / 2.15
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    zIndex: 1
  }
})
