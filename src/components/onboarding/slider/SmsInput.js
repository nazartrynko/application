import React, {Component} from 'react';
import {View, Text, TextInput, Animated, StyleSheet, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

import {colors, fonts} from 'styles';
import {height, width} from 'helpers/dimensions';

export class SmsInput extends Component {

  constructor() {
    super();
  }

  componentDidMount() {
    this.props.onRef(this);
    this.smsInput.focus();
  }

  focus() {
    this.smsInput.focus();
  }

  render() {
    const {value, handleSmsCodeValue, onFocus, animated} = this.props;

    return (
      <View style={styles.input}>
        <Animated.View style={animated}>
          <Text style={styles.smscode}>SMS code</Text>
        </Animated.View>
        <TextInput
          value={value}
          onChangeText={(value) => {
            handleSmsCodeValue(value);
          }}
          ref={(input) => { this.smsInput = input; }}
          onFocus={onFocus}
          maxLength={4}
          keyboardType="numeric"
          selectionColor={colors.black}
          underlineColorAndroid="transparent"
          autocorrect={false}
          style={styles.text}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    ...fonts._22PxGilroyMedium,
    fontSize: 22 * width / 350,
    paddingHorizontal: 15,
    textAlign: "center",
    color: colors.black,
    marginTop: -3,
    height: 50
  },
  input: {
    height: 67,
    backgroundColor: colors.white,
    borderBottomColor: colors.silver,
    borderBottomWidth: 1,
    width: width - (width / 5.76) * 2
  },
  smscode: {
    ...fonts._14PxGilroyMedium,
    textAlign: "center",
    color: colors.coolGrey
  }
});
