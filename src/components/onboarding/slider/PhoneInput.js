import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  Animated,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';

import {colors, fonts} from 'styles';
import {height, width} from 'helpers/dimensions';
import {TextInputMask} from 'react-native-masked-text';

export default class PhoneInput extends Component {

  constructor() {
    super();
  }

  componentDidMount() {
    this.props.onRef(this);
  }

  handleChangeText() {
    let value = this.refs['phoneInput'].getRawValue();
    this.props.onChangeText(value);
  }

  shouldComponentUpdate(props) {
    return props.type !== "masked";
  }

  focus() {
    this.refs['phoneInput'].getElement().focus();
  }

  render() {
    const {value, onFocus, onChangeText} = this.props;

    return (
      <View style={{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        paddingTop: 0
      }}>
        <View style={styles.countryWrapper}>
          <Text style={styles.countryCode}>+380</Text>
        </View>
        <View style={styles.phoneInputWrapper}>
          <TextInputMask onChangeText={this.handleChangeText.bind(this)} value={value} ref={'phoneInput'} maxLength={12} type={'custom'} onFocus={onFocus} autocorrect={false} keyboardType="numeric" style={styles.phoneInput} selectionColor={colors.black} underlineColorAndroid="transparent" options={{
            separator: ' ',
            mask: '99 9999 9999',
            getRawValue: (value) => this.props.onChangeText(value)
          }}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  countryWrapper: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    marginTop: 34,
    height: 41.4
  },
  flag: {
    height: 18,
    width: 24,
    backgroundColor: colors.silver,
    borderBottomColor: colors.silver,
    marginBottom: 10,
    marginLeft: 5,
    borderBottomWidth: 5
  },
  countryCode: {
    ...fonts._22PxGilroyMedium,
    fontSize: 22 * width / 380,
    paddingLeft: width / 50,
    color: colors.coolGrey
  },
  phoneInputWrapper: {
    borderBottomColor: colors.silver,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: width / 15,
    height: 51,
    alignItems: 'flex-start'
  },
  phoneInput: {
    ...fonts._22PxGilroyMedium,
    fontSize: 22 * width / 380,
    width: width * .40,
    color: colors.black,
    height: 50
  }
});
