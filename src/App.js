import React, {
  Component
} from 'react';
import {StyleSheet} from 'react-native';
import {
  Router,
  Scene
} from 'react-native-router-flux';
import {
  connect,
  Provider
} from 'react-redux';

import createStore from './store/create';
import onboarding from './containers/onboarding';

const RouterWithRedux = connect()(Router);
const store = createStore();

export default class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <RouterWithRedux navigationBarStyle={{
          backgroundColor: 'transparent',
        }}>
          {onboarding}
        </RouterWithRedux>
      </Provider>
    );
  }
}
